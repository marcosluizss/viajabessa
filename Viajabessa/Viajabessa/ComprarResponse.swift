//
//  ComprarResponse.swift
//  Viajabessa
//
//  Created by Marcos Luiz on 25/01/17.
//  Copyright © 2017 Mobicare. All rights reserved.
//

import UIKit
import ObjectMapper

class ComprarResponse: Mappable {

    var cod : Int?
    var msg : String?
    
    required init?(_ map: Map){
        
    }
        
    func mapping(map: Map) {
        cod <- map["cod"]
        msg <- map["msg"]
    }
        
}
