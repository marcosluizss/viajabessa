//
//  ViewController.swift
//  Viajabessa
//
//  Created by Marcos Luiz on 24/01/17.
//  Copyright © 2017 Mobicare. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SDWebImage

class ListaViagensViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let urlGetLista = "http://private-4d5e84-viajabessa34.apiary-mock.com/viagens"
    var listaViagens : NSMutableArray = NSMutableArray()
    var dadosRequest : [String:AnyObject] = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self

        dadosRequest["dadosAparelho"] = ["versaoIos":UIDevice.currentDevice().systemVersion, "versaoAparelho":UIDevice.currentDevice().model]
        
        Alamofire.request(.GET, urlGetLista, parameters: dadosRequest ).responseArray{ (response: Response<[Viagem], NSError>) in
            
            let viagens = response.result.value
            
            if viagens != nil {
                for viagem in viagens! {
                    self.listaViagens.addObject(viagem)
                }
            }
            
            self.tableView.reloadData()
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaViagens.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("viagemCell", forIndexPath: indexPath) as! ViagemTableViewCell
        
        let viagem = listaViagens[indexPath.row] as! Viagem
        cell.nome.text = viagem.nome
        cell.valor.text = "R$ " + String(format: "%.2f", viagem.valor!).stringByReplacingOccurrencesOfString(".", withString: ",")
        cell.imagem.sd_setImageWithURL(NSURL(string: (viagem.urlImagem)!))
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let viagem = listaViagens.objectAtIndex(indexPath.row) as! Viagem
        
        performSegueWithIdentifier("exibeViagem", sender: viagem)
        
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "exibeViagem" {
            
            let viagemView = segue.destinationViewController as! ViagemViewController
            viagemView.viagem = sender as? Viagem
        }
    }
}

