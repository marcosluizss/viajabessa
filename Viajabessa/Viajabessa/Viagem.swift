//
//  Viagem.swift
//  Viajabessa
//
//  Created by Marcos Luiz on 24/01/17.
//  Copyright © 2017 Mobicare. All rights reserved.
//

import UIKit
import ObjectMapper

class Viagem: Mappable {

    var id : Int?
    var nome : String?
    var valor : Double?
    var urlImagem : String?
    var descricao : String?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        nome <- map["nome"]
        valor <- map["valor"]        
        descricao <- map["descricao"]
        urlImagem <- map["imagem"]
    }

    
}
