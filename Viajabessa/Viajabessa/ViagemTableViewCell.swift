//
//  ViagemTableViewCell.swift
//  Viajabessa
//
//  Created by Marcos Luiz on 24/01/17.
//  Copyright © 2017 Mobicare. All rights reserved.
//

import UIKit

class ViagemTableViewCell: UITableViewCell {

    @IBOutlet weak var imagem: UIImageView!
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var valor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
