//
//  ViagemViewController.swift
//  Viajabessa
//
//  Created by Marcos Luiz on 24/01/17.
//  Copyright © 2017 Mobicare. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SDWebImage

let urlAcaoViagem = "http://private-4d5e84-viajabessa34.apiary-mock.com/viagem/id"

class ViagemViewController: UIViewController {

    @IBOutlet weak var imagem: UIImageView!
    @IBOutlet weak var valor: UILabel!
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var descricao: UILabel!
    
    var viagem : Viagem?
    var comprarResponse : ComprarResponse?
    var dadosRequest : [String:AnyObject] = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dadosRequest["dadosAparelho"] = ["versaoIos":UIDevice.currentDevice().systemVersion, "versaoAparelho":UIDevice.currentDevice().model]
        
        Alamofire.request(.GET, urlAcaoViagem , parameters: dadosRequest ).responseObject{ (response: Response<Viagem, NSError>) in
            
            if let viagem = response.result.value {
                self.nome.text = viagem.nome
                self.valor.text = "R$ " + String(format: "%.2f", viagem.valor!).stringByReplacingOccurrencesOfString(".", withString: ",")
                self.imagem.sd_setImageWithURL(NSURL(string: (viagem.urlImagem)!) )
                self.descricao.text = viagem.descricao
            }
            
            
        }
        
        
    }

    @IBAction func voltar(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func comprarViagem(sender: AnyObject) {
        dadosRequest["dadosUsuario"] = ["id":1, "nome":"Marcos Luiz"]
        
        Alamofire.request(.POST, urlAcaoViagem , parameters: dadosRequest ).responseObject{ (response: Response<ComprarResponse, NSError>) in
            
            if let comprarResponse = response.result.value {
                var title = ""
                
                if comprarResponse.cod == 0 {
                    title = "Parabéns"
                }else{
                    title = "Erro"
                }
                
                let alertController = UIAlertController(title: title, message: comprarResponse.msg, preferredStyle: .Alert)
                let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            
            
        }
    }

}
